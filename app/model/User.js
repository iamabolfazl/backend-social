const mongoose = require('mongoose')

const Schema = mongoose.Schema

const User = mongoose.Schema({
    name:{type: String, required: true},
    email: { type: String, required: true },
    password: { type: String, required: true},
    image: { type: String, default: ''},
    posts:[{type: Schema.Types.ObjectId, ref: 'Post'}]

})

module.exports = mongoose.model('User', User)