const express = require('express')
const app = express()
const http = require('http')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const session = require('express-session');
const MongoStore = require('connect-mongo');

module.exports = class Application{
    constructor(){
        this.configServer()
        this.configAll()
        this.configRoutes()
        this.configDatabase()
    }


    configServer(){
        const server = http.createServer(app)
        server.listen(8000, (err)=>{
            if(err) console.log(err)
            console.log('Server Run on Port 8000')
        })
    }

    configDatabase(){
        mongoose.Promise = global.Promise
        mongoose.connect('mongodb://localhost/Abiium',  { 
            useNewUrlParser: true ,
            useUnifiedTopology: true  
        })

    }

    configAll(){
        app.use(express.json());
        app.use(express.urlencoded());
        app.use((req, res, next)=>{
            console.log('middleware working')
            next()
        })
        app.use(session({
            secret: 'secretID',
            resave: false,
            unset: 'destroy',
            saveUninitialized: false,
            store: MongoStore.create({ mongoUrl: 'mongodb://localhost/Abiium'}) 
        }))    

    }


    configRoutes(){
        app.use(require('./routes'))
    }
}

