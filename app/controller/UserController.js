const Controller = require('./Controller')
const User = require('./../model/User')
const HttpError = require('./../model/Http-Error')
class UserController extends Controller {

    async register(req, res, next) {

        let existingUser

        try {
            existingUser = await User.findOne({ email: req.body.email })
        } catch (err) {
            console.log(err)
            const error = new HttpError('Sing up faild.', 500)
            return next(error)
        }

        if (existingUser) {
            const error = new HttpError('User exist.', 422)
            return next(error)
        }

        const createdUser = new User({
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
            image: '',
            posts: []
        })

        try {
            await createdUser.save()
        } catch (err) {
            console.log(err)
            const error = new HttpError('register faild.', 500)
            return next(error)
        }

        res.status(201).json({ user: createdUser.toObject({ getters: true }) })
    }


    async login(req, res, next){
        const {email, password} = req.body

        let existedUser

        try {
            existedUser = await User.findOne({email: email})
        } catch (err) {
            const error = new HttpError('User Is Not Register', 500)
            return next(error)
        }
        if(!existedUser || existedUser.password !== password){
            const error = new HttpError('OPS! Its Was Problem', 500)
            return next(error)
        }
        res.status(200).json({mesages: 'Logged In'})
    }


    async getUser(req, res, next){
        let users
        try {
            //همه اطلاعات یک یوزر رو میدهد به جز  یسورد
            users = await User.findById(req.params.id, '-password')
        } catch (err) {
            const error = new HttpError('مشکلی به وجود امده است.', 422)
            return next(error)
        }
        res.status(200).json({user: users})
    }

}


module.exports = new UserController