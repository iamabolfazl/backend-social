const express = require('express')

const router = express.Router()

const UserController = require('./../../controller/UserController')

router.post('/api/v1/register', UserController.register)
router.post('/api/v1/login', UserController.login)
router.get('/api/v1/getuser/:id', UserController.getUser)

module.exports = router