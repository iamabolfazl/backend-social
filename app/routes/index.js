const express = require('express')

const router = express.Router()

const UserRoute = require('./User/UserRoutes')

router.use('/user', UserRoute)



module.exports = router